﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistenta
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            Console.Write("Introduceti nr: ");
            n = int.Parse(Console.ReadLine());
            Console.WriteLine(Persistenta(n));
            Console.ReadKey();

        }
         static int Persistenta(int n)
        {
            int c = n;
            int produs = 1;
            int pers = 0;
            while(c!=0)
            {
                produs *= c % 10;
                c /= 10;
            }
            pers++;
            while(produs > 9)
            {
                c = produs;
                produs = 1;
                while(c!=0)
                {
                    produs *= c % 10;
                    c /= 10;
                }
                pers++;
            }
            return pers;
        }
    }
}
